package com.htc.payroll.payrollservice;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.payroll.payrollservice.exception.EmptyException;
import com.htc.payroll.payrollservice.model.DiscountsModel;
import com.htc.payroll.payrollservice.model.RetentionModel;
import com.htc.payroll.payrollservice.repository.DiscountsRepository;
import com.htc.payroll.payrollservice.service.DiscountService;
import com.htc.payroll.payrollservice.service.RetentionService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DiscountTest {
	
	@Autowired
	DiscountsRepository discountRepository;
	
	@Autowired
	DiscountService dis;
	
	@Autowired
	RetentionService rt;

	@Test
	public void test() throws EmptyException {
//		for (DiscountsModel dM : discountRepository.findAll()) {
//			System.out.println(dM.toString());
//		}
		for (RetentionModel dModel : rt.loadAllEnableRetention(true)) {
			System.out.println(dModel.toString());
		}
		
	}

}
