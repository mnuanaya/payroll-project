package com.htc.payroll.payrollservice.util;

public final class Constants {
	public static final String IVA = "constant.iva";
	
	public static final String SUCCESS_CODE = "successCode";
	public static final String MESSAGE_SUCCESS = "messageSucces";
	
	public static final String ERROR_CODE = "errorCode";
	public static final String MESSAGE_ERROR = "messageError";
	
	public static final String NULL_CODE = "nullCode";
	public static final String MESSAGE_NULL = "messageNull";
	
	public static final String EMPTY_CODE = "emptyCode";
	public static final String MESSAGE_EMPTY = "messageEmpty";
	
	public static final String FECHA_CODE = "fechaIgualCode";
	public static final String MESSAGE_FECHA = "fechaIgualMessage";
	
	public static final String FECHA_FUTURA = "fechaFuturaCode";
	public static final String MESSAGE_FECHA_FUTURA = "messageFechaFutura";
	
	public static final String POSITIVE_CODE = "positiveCode";
	public static final String MESSAGE_POSITIVE = "messagePositive";
	
	public static final String NOTFOUND_CODE = "notFound";
	public static final String MESSAGE_NOTFOUND = "messageNotFound";
	
	public static final String VACIO_CODE = "vaciaCode";
	public static final String MESSAGE_VACIO = "messageVaciaCode";
	
	public static final String PAYROLL_NOT_FOUND_CODE = "payrollNotFoundCode";
	public static final String MESSAGE_NOT_FOUND_PAYROLL = "messagePayrollNotFound";
	
	public static final String PAYROLL_NULL_CODE = "payrollNullCode";
	public static final String MESSAGE_PAYROLL_NULL = "messagePayrollNull";
	
	public static final String EMPLEADO_NULL_CODE = "empleadoNullCode";
	public static final String MESSAGE_EMPLEADO_NULL = "messageEmpleadoNullCode";
	
	public static final String COMPANY_NULL_CODE = "companyNullCode";
	public static final String MESSAGE_COMPANY_NULL = "messageCompanyNullCode";
	
	public static final String NO_ANULAR_CODE = "noAnularCode";
	public static final String MESSAGE_NO_ANULAR = "messageNoAnular";
	
	public static final String FECHA_NULL_CODE = "fechaNullCode";
	public static final String MESSAGE_FECHA_NULL = "messageFechaNull";
	
	public static final String TIPO_PAY_NULL_CODE = "tipoPayrollCode";
	public static final String MESSAGE_TIPO_PAY_NULL = "messageTipoPayNull";
	
	public static final String TIPO_PAY_NOT_FOUND_CODE = "tipoPayNotFound";
	public static final String MESSAGE_TIPO_PAY_FOUND = "messageTipoPayFound";
	
	public static final String CONTRACT_TYPE_NOT_FOUND_CODE = "contractTypeNotFound";
	public static final String MESSAGE_CONTRACT_NOT_FOUND = "messageContractTypeNot";
	
	public static final String POSITIVE_OTHER_INCOME_CODE = "positiveIncomeCode";
	public static final String MESSAGE_POSITIVE_OTHER_INCOME = "messagePositiveIncome";
	
	public static final String POSITIVE_OTHER_DISCOUNT_CODE = "positiveDiscountCode";
	public static final String MESSAGE_POSITIVE_OTHER_DISCOUNT = "messagePositiveDiscount";
	
//	public static final String MESSAGE_OBSERVATION_INSERT = "messageObservacionInsert";
//	public static final String MESSAGE_OBSERVATION_UPDATE = "messageObservacionUpdate";
//	
//	public static final String MESSAGE_OBSERVATION_ANULAR = "messageObservacionAnular";
	
	
	
	
	

}
