package com.htc.payroll.payrollservice.dao;

import java.sql.Date;
import java.util.List;

import com.htc.payroll.payrollservice.model.PayrollModel;

public interface PayrollDao {
	
	public boolean insert(PayrollModel payroll);

	public Integer insertAndReturn(PayrollModel payroll);

	public boolean inserBatch(List<PayrollModel> payrolls);

	public List<PayrollModel> loadAllPayrolls();
	
	public List<PayrollModel> loadPayrollDate(String mes, long a�o);

	public PayrollModel findPayrollById(long id);
	
	public PayrollModel findPayrollByEmployee(long id);
	
	public PayrollModel findPayrollByDate(Date date);

	public boolean updatePayroll(Integer id, PayrollModel payroll);

	public boolean updateStatePayroll(Integer id, PayrollModel pm);

	public boolean deletePayroll(PayrollModel id);

}
