package com.htc.payroll.payrollservice.dao.impl;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.htc.payroll.payrollservice.dao.PayrollDao;
import com.htc.payroll.payrollservice.model.PayrollModel;

@Repository
public class PayrollDaoImpl extends JdbcDaoSupport implements PayrollDao{
	
	@Autowired
	public DataSource dataSource;

	@Autowired
	public Environment env;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Transactional
	@Override
	public boolean insert(PayrollModel payroll) {
		
		String sql = "INSERT INTO payroll "
				+ "(payroll_date,salary, other_income, subtotal_salary, isss, afp,isr,other_discount, id_company, total_income, total_discount, total_salary, observation, enable) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		return getJdbcTemplate().update(sql, new Object[] {payroll.getPayrollDate(),
				payroll.getSalary(), payroll.getOtherIncome(), payroll.getSubtotalSalary(), payroll.getIsss(), payroll.getAfp(),
				payroll.getIsr(), payroll.getOtherDiscount(), payroll.getIdCompany(),
				payroll.getTotalIncome(), payroll.getTotalDiscount(), payroll.getTotalSalary(),
				payroll.getObservation(), payroll.isEnable()

		}) > 0;
		
	}

	@Transactional
	@Override
	public Integer insertAndReturn(PayrollModel payroll) {
		
		final String INSERT_SQL = "INSERT INTO payroll "
				+ "(payroll_date,salary, other_income, subtotal_salary, isss, afp,isr,other_discount, id_company, total_income, total_discount, total_salary, observation, enable) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(java.sql.Connection arg0) throws SQLException {
				PreparedStatement ps = arg0.prepareStatement(INSERT_SQL, new String[] { "id" });

				ps.setDate(1,  payroll.getPayrollDate());
				ps.setDouble(2, payroll.getSalary());
				ps.setDouble(3, payroll.getOtherIncome());
				ps.setDouble(4, payroll.getSubtotalSalary());
				ps.setDouble(5, payroll.getIsss());
				ps.setDouble(6, payroll.getAfp());
				ps.setDouble(7, payroll.getIsr());
				ps.setDouble(8, payroll.getOtherDiscount());
				ps.setInt(9, payroll.getIdCompany());
				ps.setDouble(10, payroll.getTotalIncome());
				ps.setDouble(11, payroll.getTotalDiscount());
				ps.setDouble(12, payroll.getTotalSalary());
				ps.setString(13, payroll.getObservation());
				ps.setBoolean(14, payroll.isEnable());

				return ps;
			}

		}, keyHolder);
		return (Integer) keyHolder.getKey(); // now contains the generated key
	}

	@Transactional
	@Override
	public boolean inserBatch(List<PayrollModel> payrolls) {
		String sql = "INSERT INTO payroll "
				+ "(payroll_date,salary, other_income, subtotal_salary, isss, afp,isr,other_discount, id_company, total_income, total_discount, total_salary, observation, enable) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				PayrollModel payroll = payrolls.get(i);
				
				ps.setDate(1,  payroll.getPayrollDate());
				ps.setDouble(2, payroll.getSalary());
				ps.setDouble(3, payroll.getOtherIncome());
				ps.setDouble(4, payroll.getSubtotalSalary());
				ps.setDouble(5, payroll.getIsss());
				ps.setDouble(6, payroll.getAfp());
				ps.setDouble(7, payroll.getIsr());
				ps.setDouble(8, payroll.getOtherDiscount());
				ps.setInt(9, payroll.getIdCompany());
				ps.setDouble(10, payroll.getTotalIncome());
				ps.setDouble(11, payroll.getTotalDiscount());
				ps.setDouble(12, payroll.getTotalSalary());
				ps.setString(13, payroll.getObservation());
				ps.setBoolean(14, payroll.isEnable());


			}

			public int getBatchSize() {
				return payrolls.size();
			}
		});

		return Boolean.TRUE;
	}

	@Override
	public List<PayrollModel> loadAllPayrolls() {
		
		String sql = "SELECT * FROM payroll";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<PayrollModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			PayrollModel payrolls = new PayrollModel();
			
			payrolls.setId((Integer) row.get("id"));
			payrolls.setPayrollDate((Date) row.get("payroll_date"));
			payrolls.setSalary((Double) row.get("salary"));
			payrolls.setOtherIncome((Double) row.get("other_income"));
			payrolls.setSubtotalSalary((Double) row.get("subtotal_salary"));
			payrolls.setIsss((Double) row.get("isss"));
			payrolls.setAfp((Double) row.get("afp"));
			payrolls.setIsr((Double) row.get("isr"));
			payrolls.setOtherDiscount((Double) row.get("other_discount"));
			payrolls.setIdCompany((Integer) row.get("id_company"));
			payrolls.setTotalIncome((Double) row.get("total_income"));
			payrolls.setTotalDiscount((Double) row.get("total_discount"));
			payrolls.setTotalSalary((Double) row.get("total_salary"));
			payrolls.setObservation((String) row.get("observation"));
			payrolls.setEnable((boolean) row.get("enable"));
			

			result.add(payrolls);

		}

		return result;

	}

	@Override
	public PayrollModel findPayrollById(long id) {
		String sql = "SELECT * FROM payroll WHERE id = ?";
		List<PayrollModel> list = getJdbcTemplate().query(sql, new Object[] { id }, new RowMapper<PayrollModel>() {
			@Override
			public PayrollModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				PayrollModel payroll = new PayrollModel();
				payroll.setId(rs.getInt("id"));
				payroll.setPayrollDate(rs.getDate("payroll_date"));
				payroll.setSalary(rs.getDouble("salary"));
				payroll.setOtherIncome(rs.getDouble("other_income"));
				payroll.setSubtotalSalary(rs.getDouble("subtotal_salary"));
				payroll.setIsss(rs.getDouble("isss"));
				payroll.setAfp(rs.getDouble("afp"));
				payroll.setIsr(rs.getDouble("isr"));
				payroll.setOtherDiscount(rs.getDouble("other_discount"));
				payroll.setIdCompany(rs.getInt("id_company"));
				payroll.setTotalIncome(rs.getDouble("total_income"));
				payroll.setTotalDiscount(rs.getDouble("total_discount"));
				payroll.setTotalSalary(rs.getDouble("total_salary"));
				payroll.setObservation(rs.getString("observation"));
				payroll.setEnable(rs.getBoolean("enable"));

				return payroll;
			}
		});

		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	@Override
	public boolean updatePayroll(Integer id, PayrollModel payroll) {
		
		return false;
	}

	@Override
	public boolean updateStatePayroll(Integer id, PayrollModel pm) {
		
		return false;
	}

	@Override
	public boolean deletePayroll(PayrollModel id) {
		
		return false;
	}

	@Override
	public PayrollModel findPayrollByEmployee(long id) {
		/*
		 * SELECT id_product, d.id_bill
			FROM detail d JOIN bill b ON d.id_bill = b.id_bill
			WHERE d.id_product = 3
		 */
		/*String sql = "SELECT * FROM payroll WHERE id_employee = ?";
		List<PayrollModel> list = getJdbcTemplate().query(sql, new Object[] { id }, new RowMapper<PayrollModel>() {
			@Override
			public PayrollModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				PayrollModel payroll = new PayrollModel();
				payroll.setId(rs.getInt("id"));
				payroll.setPayrollDate(rs.getDate("payroll_date"));
				payroll.setIdEmployee(rs.getInt("id_employee"));
				payroll.setSalary(rs.getDouble("salary"));
				payroll.setOtherIncome(rs.getDouble("other_income"));
				payroll.setSubtotalSalary(rs.getDouble("subtotal_salary"));
				payroll.setIsss(rs.getDouble("isss"));
				payroll.setAfp(rs.getDouble("afp"));
				payroll.setIsr(rs.getDouble("isr"));
				payroll.setOtherDiscount(rs.getDouble("other_discount"));
				payroll.setTotalSalary(rs.getDouble("total_salary"));

				return payroll;
			}
		});

		if (list.isEmpty())
			return null;
		return list.get(0);*/
		return null;
	}

	@Override
	public PayrollModel findPayrollByDate(Date date) {
		String sql = "SELECT * FROM payroll WHERE payroll_date = ?";
		List<PayrollModel> list = getJdbcTemplate().query(sql, new Object[] { date }, new RowMapper<PayrollModel>() {
			@Override
			public PayrollModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				PayrollModel payroll = new PayrollModel();
				payroll.setId(rs.getInt("id"));
				payroll.setPayrollDate(rs.getDate("payroll_date"));
				payroll.setSalary(rs.getDouble("salary"));
				payroll.setOtherIncome(rs.getDouble("other_income"));
				payroll.setSubtotalSalary(rs.getDouble("subtotal_salary"));
				payroll.setIsss(rs.getDouble("isss"));
				payroll.setAfp(rs.getDouble("afp"));
				payroll.setIsr(rs.getDouble("isr"));
				payroll.setOtherDiscount(rs.getDouble("other_discount"));
				payroll.setIdCompany(rs.getInt("id_company"));
				payroll.setTotalIncome(rs.getDouble("total_income"));
				payroll.setTotalDiscount(rs.getDouble("total_discount"));
				payroll.setTotalSalary(rs.getDouble("total_salary"));
				payroll.setObservation(rs.getString("observation"));
				payroll.setEnable(rs.getBoolean("enable"));

				return payroll;
			}
		});

		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	@Override
	public List<PayrollModel> loadPayrollDate(String mes, long a�o) {
		
		String sql = "SELECT * FROM payroll WHERE TO_CHAR(payroll_date,'MM')='"+mes+"' AND TO_CHAR(payroll_date,'YYYY')='"+a�o+"'";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<PayrollModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			PayrollModel payrolls = new PayrollModel();
			
			payrolls.setId((Integer) row.get("id"));
			payrolls.setPayrollDate((Date) row.get("payroll_date"));
			payrolls.setSalary((Double) row.get("salary"));
			payrolls.setOtherIncome((Double) row.get("other_income"));
			payrolls.setSubtotalSalary((Double) row.get("subtotal_salary"));
			payrolls.setIsss((Double) row.get("isss"));
			payrolls.setAfp((Double) row.get("afp"));
			payrolls.setIsr((Double) row.get("isr"));
			payrolls.setOtherDiscount((Double) row.get("other_discount"));
			payrolls.setIdCompany((Integer) row.get("id_company"));
			payrolls.setTotalIncome((Double) row.get("total_income"));
			payrolls.setTotalDiscount((Double) row.get("total_discount"));
			payrolls.setTotalSalary((Double) row.get("total_salary"));
			payrolls.setObservation((String) row.get("observation"));
			payrolls.setEnable((boolean) row.get("enable"));
			

			result.add(payrolls);

		}

		return result;
		
	}

}
