package com.htc.payroll.payrollservice.controller;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htc.payroll.payrollservice.dto.PayrollDto;
import com.htc.payroll.payrollservice.exception.EmptyException;
import com.htc.payroll.payrollservice.exception.NullObjectException;
import com.htc.payroll.payrollservice.exception.OperationException;
import com.htc.payroll.payrollservice.model.PayrollModel;
import com.htc.payroll.payrollservice.service.PayrollService;

@RestController
@RequestMapping("/payroll")
public class PayrollController {
	
	@Autowired
	PayrollService payrollService;
	
	@Autowired
	  Mapper mapper;
	
	
//	@PostMapping("/")
//	public PayrollModel insertPayroll(@Valid @RequestBody PayrollDto payrollDto) throws OperationException, NullObjectException,EmptyException {
//
//		
//		PayrollModel payDto = mapper.map(payrollDto, PayrollModel.class);
//		
//		HttpHeaders headers = new HttpHeaders();
//
//			payrollService.insert(payDto);
//
//
//			return payDto;
//
//		}
	
	@PostMapping("/")
	public ResponseEntity<Object> InsertPayroll(@Valid @RequestBody PayrollDto payrollDto) throws OperationException, NullObjectException,EmptyException {

		
		PayrollModel payDto = mapper.map(payrollDto, PayrollModel.class);
		
		HttpHeaders headers = new HttpHeaders();

			payrollService.insert(payDto);


			return new ResponseEntity<>(headers, HttpStatus.OK);

		}

}
