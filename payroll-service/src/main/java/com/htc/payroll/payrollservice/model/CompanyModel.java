package com.htc.payroll.payrollservice.model;

import java.io.Serializable;

public class CompanyModel implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String nit;
	private String phoneNumber;
	private String sector;
	
	public CompanyModel() {
	
	}

	public CompanyModel(Integer id, String name, String nit, String phoneNumber, String sector) {
		super();
		this.id = id;
		this.name = name;
		this.nit = nit;
		this.phoneNumber = phoneNumber;
		this.sector = sector;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getNit() {
		return nit;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getSector() {
		return sector;
	}
	
	

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	@Override
	public String toString() {
		return "CompanyModel [id=" + id + ", name=" + name + ", nit=" + nit + ", phoneNumber=" + phoneNumber
				+ ", sector=" + sector + "]";
	}

	
	
}
