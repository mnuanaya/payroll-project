package com.htc.payroll.payrollservice.dao.impl;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.htc.payroll.payrollservice.dao.PayrollDetailDao;
import com.htc.payroll.payrollservice.model.PayrollDetailModel;

@Repository
public class PayrollDetailDaoImpl extends JdbcDaoSupport implements PayrollDetailDao {
	
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public Integer insertAndReturn(PayrollDetailModel detail) {
		
		/*final String INSERTSQL = "INSERT INTO payroll_detail "
				+ "((id_detail, id_bill, id_product , cantidad, subtotal) VALUES (?, ?, ?, ?, ?)";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(java.sql.Connection arg0) throws SQLException {
				PreparedStatement ps = arg0.prepareStatement(INSERTSQL, new String[] { "id_detail" });

				ps.setInt(1, detail.getIdDetail());
				ps.setInt(2, detail.getIdBill());
				ps.setInt(3, detail.getIdProduct());
				ps.setInt(4, detail.getCantidad());
				ps.setDouble(5, detail.getSubTotal());
				return ps;
			}

		}, keyHolder);
		return (Integer) keyHolder.getKey(); // now contains the generated key*/
		return null;
	}

	@Override
	public boolean inserBatch(List<PayrollDetailModel> details, Integer idPayroll) {
		
		String sql = "INSERT INTO payroll_detail " + "(id_payroll, payroll_date, id_payroll_type, nominal_salary, other_income, other_discount, isss, afp, isr, vigency_discounts, vigency_retention, id_employee, id_company, total_income, total_discount, total_salary) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				
				PayrollDetailModel detail = details.get(i);

				ps.setInt(1, idPayroll);
				ps.setDate(2, detail.getPayrollDate());
				ps.setInt(3, detail.getIdPayrollType());
				ps.setDouble(4, detail.getNominalSalary());
				ps.setDouble(5, detail.getOtherIncome());
				ps.setDouble(6, detail.getOtherDiscount());
				ps.setDouble(7, detail.getIsss());
				ps.setDouble(8, detail.getAfp());
				ps.setDouble(9, detail.getIsr());
				ps.setString(10, detail.getVigencyDiscounts());
				ps.setString(11, detail.getVigencyRetention());
				ps.setInt(12, detail.getIdEmployee());
				ps.setInt(13, detail.getIdCompany());
				ps.setDouble(14, detail.getTotalIncome());
				ps.setDouble(15, detail.getTotalDiscount());
				ps.setDouble(16, detail.getTotalSalary());
				
				

			}

			public int getBatchSize() {
				return details.size();
			}
		});

		return Boolean.TRUE;
	}

	@Override
	public List<PayrollDetailModel> DetailById(Integer detid) {
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM payroll_detail where id= ").append(detid);
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql.toString());

		List<PayrollDetailModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			PayrollDetailModel details = new PayrollDetailModel();
			
			details.setId((Integer) row.get("id"));
			details.setIdPayroll((Integer) row.get("id_payroll"));
			details.setPayrollDate((Date) row.get("payroll_date"));
			details.setIdPayrollType((Integer) row.get("id_payroll_type"));
			details.setNominalSalary((Double) row.get("nominal_salary"));
			details.setOtherIncome((Double) row.get("other_income"));
			details.setOtherDiscount((Double) row.get("other_discount"));
			details.setIsss((Double) row.get("isss"));
			details.setAfp((Double) row.get("afp"));
			details.setIsr((Double) row.get("isr"));
			details.setVigencyDiscounts((String) row.get("vigency_discounts"));
			details.setVigencyRetention((String) row.get("vigency_retention"));
			details.setIdEmployee((Integer) row.get("id_employee"));
			details.setIdCompany((Integer) row.get("id_company"));
			details.setTotalIncome((Double) row.get("total_income"));
			details.setTotalDiscount((Double) row.get("total_discount"));
			details.setTotalSalary((Double) row.get("total_salary"));

			result.add(details);
		}

		return result;
	}

	@Override
	public List<PayrollDetailModel> loadAllDetailInPayroll(Integer id) {
		
		String sql = "SELECT * FROM payroll_detail where id_payroll= ?";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql, new Object[] {id});

		List<PayrollDetailModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			PayrollDetailModel details = new PayrollDetailModel();

			details.setId((Integer) row.get("id"));
			details.setIdPayroll((Integer) row.get("id_payroll"));
			details.setPayrollDate((Date) row.get("payroll_date"));
			details.setIdPayrollType((Integer) row.get("id_payroll_type"));
			details.setNominalSalary((Double) row.get("nominal_salary"));
			details.setOtherIncome((Double) row.get("other_income"));
			details.setOtherDiscount((Double) row.get("other_discount"));
			details.setIsss((Double) row.get("isss"));
			details.setAfp((Double) row.get("afp"));
			details.setIsr((Double) row.get("isr"));
			details.setVigencyDiscounts((String) row.get("vigency_discounts"));
			details.setVigencyRetention((String) row.get("vigency_retention"));
			details.setIdEmployee((Integer) row.get("id_employee"));
			details.setIdCompany((Integer) row.get("id_company"));
			details.setTotalIncome((Double) row.get("total_income"));
			details.setTotalDiscount((Double) row.get("total_discount"));
			details.setTotalSalary((Double) row.get("total_salary"));

			result.add(details);
		}

		return result;
	}

}
