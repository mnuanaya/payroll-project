package com.htc.payroll.payrollservice.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.htc.payroll.payrollservice.dao.EmployeeDao;
import com.htc.payroll.payrollservice.model.CompanyModel;
import com.htc.payroll.payrollservice.model.EmployeeModel;

@Repository
public class EmployeeDaoImpl  extends JdbcDaoSupport implements EmployeeDao {
	
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<EmployeeModel> loadAllEmployee() {
		String sql = "SELECT * FROM employee";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<EmployeeModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			EmployeeModel employee = new EmployeeModel();
			
			employee.setId((Integer) row.get("id"));
			employee.setNames((String) row.get("names"));
			employee.setBornDate((Date) row.get("born_date"));
			employee.setIsssNumber((String) row.get("isss_number"));
			employee.setAfpNumber((String) row.get("afp_number"));
			employee.setPosition((String) row.get("position"));
			employee.setIncorporationDate((Date) row.get("incorporation_date"));
			employee.setIdContractType((Integer) row.get("id_contract_type"));

			result.add(employee);
		}

		return result;
	}

	@Override
	public EmployeeModel findEmployeeById(long id) {
		String sql = "SELECT * FROM employee WHERE id = ?";
		List<EmployeeModel> list = getJdbcTemplate().query(sql, new Object[] { id }, new RowMapper<EmployeeModel>() {
			@Override
			public EmployeeModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				EmployeeModel employee = new EmployeeModel();
				
				employee.setId(rs.getInt("id"));
				employee.setNames(rs.getString("names"));
				employee.setBornDate(rs.getDate("born_date"));
				employee.setIsssNumber(rs.getString("isss_number"));
				employee.setAfpNumber(rs.getString("afp_number"));
				employee.setPosition(rs.getString("position"));
				employee.setIncorporationDate(rs.getDate("incorporation_date"));
				employee.setIdContractType(rs.getInt("id_contract_type"));
				
				

				return employee;
			}
		});
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

}
