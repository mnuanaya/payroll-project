package com.htc.payroll.payrollservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.htc.payroll.payrollservice.model.DiscountsModel;

public interface DiscountsRepository extends JpaRepository<DiscountsModel, Integer>{

}
