package com.htc.payroll.payrollservice.model;

import java.io.Serializable;
import java.sql.Date;

public class RetentionModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer strench;
	private double from;
	private double to;
	private double porcentApply;
	private double overExcess;
	private double fixedFee;
	private boolean juneRecalculation;
	private boolean decemberRecalculation;
	private Date vigencyDate;
	private boolean enable;
	
	public RetentionModel() {
		
	}

	public RetentionModel(Integer id, Integer strench, double from, double to, double porcentApply, double overExcess,
			double fixedFee, boolean juneRecalculation, boolean decemberRecalculation, Date vigencyDate,
			boolean enable) {
		super();
		this.id = id;
		this.strench = strench;
		this.from = from;
		this.to = to;
		this.porcentApply = porcentApply;
		this.overExcess = overExcess;
		this.fixedFee = fixedFee;
		this.juneRecalculation = juneRecalculation;
		this.decemberRecalculation = decemberRecalculation;
		this.vigencyDate = vigencyDate;
		this.enable = enable;
	}

	public RetentionModel(Integer strench, double from, double to, double porcentApply, double overExcess,
			double fixedFee, boolean juneRecalculation, boolean decemberRecalculation, Date vigencyDate,
			boolean enable) {
		super();
		this.strench = strench;
		this.from = from;
		this.to = to;
		this.porcentApply = porcentApply;
		this.overExcess = overExcess;
		this.fixedFee = fixedFee;
		this.juneRecalculation = juneRecalculation;
		this.decemberRecalculation = decemberRecalculation;
		this.vigencyDate = vigencyDate;
		this.enable = enable;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStrench() {
		return strench;
	}

	public void setStrench(Integer strench) {
		this.strench = strench;
	}

	public double getFrom() {
		return from;
	}

	public void setFrom(double from) {
		this.from = from;
	}

	public double getTo() {
		return to;
	}

	public void setTo(double to) {
		this.to = to;
	}

	public double getPorcentApply() {
		return porcentApply;
	}

	public void setPorcentApply(double porcentApply) {
		this.porcentApply = porcentApply;
	}

	public double getOverExcess() {
		return overExcess;
	}

	public void setOverExcess(double overExcess) {
		this.overExcess = overExcess;
	}

	public double getFixedFee() {
		return fixedFee;
	}

	public void setFixedFee(double fixedFee) {
		this.fixedFee = fixedFee;
	}

	public boolean isJuneRecalculation() {
		return juneRecalculation;
	}

	public void setJuneRecalculation(boolean juneRecalculation) {
		this.juneRecalculation = juneRecalculation;
	}

	public boolean isDecemberRecalculation() {
		return decemberRecalculation;
	}

	public void setDecemberRecalculation(boolean decemberRecalculation) {
		this.decemberRecalculation = decemberRecalculation;
	}

	public Date getVigencyDate() {
		return vigencyDate;
	}

	public void setVigencyDate(Date vigencyDate) {
		this.vigencyDate = vigencyDate;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "RetentionModel [id=" + id + ", strench=" + strench + ", from=" + from + ", to=" + to + ", porcentApply="
				+ porcentApply + ", overExcess=" + overExcess + ", fixedFee=" + fixedFee + ", juneRecalculation="
				+ juneRecalculation + ", decemberRecalculation=" + decemberRecalculation + ", vigencyDate="
				+ vigencyDate + ", enable=" + enable + "]";
	}
	
	

	

}
