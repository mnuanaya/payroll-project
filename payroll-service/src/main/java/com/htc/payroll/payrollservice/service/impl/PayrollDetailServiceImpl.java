package com.htc.payroll.payrollservice.service.impl;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.htc.payroll.payrollservice.dao.PayrollDetailDao;
import com.htc.payroll.payrollservice.exception.EmptyException;
import com.htc.payroll.payrollservice.exception.NullObjectException;
import com.htc.payroll.payrollservice.exception.OperationException;
import com.htc.payroll.payrollservice.model.PayrollDetailModel;
import com.htc.payroll.payrollservice.service.PayrollDetailService;

@Component
public class PayrollDetailServiceImpl implements PayrollDetailService {
	
	@Autowired
	PayrollDetailDao payrollDetail;

	@Override
	public boolean insert(PayrollDetailModel detail) throws OperationException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Integer insertAndReturn(PayrollDetailModel detail) throws NullObjectException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean inserBatchDetail(List<PayrollDetailModel> details) throws OperationException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<PayrollDetailModel> loadAllDetails() throws EmptyException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PayrollDetailModel getDetalleById(Integer detail_id) throws NullObjectException {
		// TODO Auto-generated method stub
		return null;
	}

	

	@Override
	public PayrollDetailModel getDetail(List<Integer> idp, List<Integer> cantidad) throws NullObjectException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PayrollDetailModel getDetail(Date fechaPay, Integer tipoPay, Integer idCompany, Integer idEmployee,
			Double salary, Double otherIncome, Double otherDisc) throws NullObjectException {
		
		PayrollDetailModel detailModel = new PayrollDetailModel();
		
		detailModel.setPayrollDate(fechaPay);
		detailModel.setIdPayrollType(tipoPay);
		detailModel.setIdCompany(idCompany);
		detailModel.setIdEmployee(idEmployee);
		detailModel.setNominalSalary(salary);
		detailModel.setOtherIncome(otherIncome);
		detailModel.setOtherDiscount(otherDisc);
		return detailModel;
	}



}
