package com.htc.payroll.payrollservice.service;

import java.util.List;

import com.htc.payroll.payrollservice.exception.EmptyException;
import com.htc.payroll.payrollservice.exception.NullObjectException;
import com.htc.payroll.payrollservice.model.DiscountsModel;

public interface DiscountService {
	
	public DiscountsModel findDiscountByEnable(Boolean enable) throws NullObjectException;
	public List<DiscountsModel> loadAllDiscountByEnable(Boolean enable) throws EmptyException;

}
