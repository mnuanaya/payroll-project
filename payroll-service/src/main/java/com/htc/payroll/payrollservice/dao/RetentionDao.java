package com.htc.payroll.payrollservice.dao;

import java.sql.Date;
import java.util.List;

import com.htc.payroll.payrollservice.model.RetentionModel;

public interface RetentionDao {
	

	public RetentionModel findRetentionByEnable(Boolean enable);
	public List<RetentionModel> loadAllRetentionByEnable(Boolean enable);
	

}
