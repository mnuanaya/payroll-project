package com.htc.payroll.payrollservice.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.htc.payroll.payrollservice.dao.RetentionDao;
import com.htc.payroll.payrollservice.exception.EmptyException;
import com.htc.payroll.payrollservice.exception.NullObjectException;
import com.htc.payroll.payrollservice.model.DiscountsModel;
import com.htc.payroll.payrollservice.model.RetentionModel;
import com.htc.payroll.payrollservice.service.RetentionService;
import com.htc.payroll.payrollservice.util.Constants;

@Service
public class RetentionServiceImpl implements RetentionService {

	@Autowired
	RetentionDao retentionDao;

	@Autowired
	Environment env;

	@Override
	public RetentionModel findEnableRetention(Boolean enable) throws NullObjectException {

		RetentionModel retentionModel = retentionDao.findRetentionByEnable(enable);
		if (retentionModel == null) {
			throw new NullObjectException(Long.valueOf(env.getProperty(Constants.EMPTY_CODE)),
					env.getProperty(Constants.MESSAGE_EMPTY));
		}

		return retentionModel;
	}

	@Override
	public List<RetentionModel> loadAllEnableRetention(Boolean enable) throws EmptyException {
		List<RetentionModel> listRetention = retentionDao.loadAllRetentionByEnable(enable);

		if (listRetention.isEmpty()) {
			throw new EmptyException(env.getProperty(Constants.MESSAGE_EMPTY));
		}
		return listRetention;
	}

}
