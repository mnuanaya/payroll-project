package com.htc.payroll.payrollservice.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.htc.payroll.payrollservice.dao.RetentionDao;
import com.htc.payroll.payrollservice.model.DiscountsModel;
import com.htc.payroll.payrollservice.model.RetentionModel;

@Repository
public class RetentionDaoImpl extends JdbcDaoSupport implements RetentionDao {
	
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public RetentionModel findRetentionByEnable(Boolean enable) {
		
		String sql = "SELECT * FROM retention WHERE enable = ?";
		List<RetentionModel> list = getJdbcTemplate().query(sql, new Object[] { enable },
				new RowMapper<RetentionModel>() {

					@Override
					public RetentionModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
						RetentionModel retention = new RetentionModel();
						
						retention.setId(rs.getInt("id"));
						retention.setStrench(rs.getInt("strench"));
						retention.setFrom(rs.getDouble("from"));
						retention.setTo(rs.getDouble("to"));
						retention.setPorcentApply(rs.getDouble("porcent_apply"));
						retention.setOverExcess(rs.getDouble("over_Excess"));
						retention.setFixedFee(rs.getDouble("fixed_fee"));
						retention.setJuneRecalculation(rs.getBoolean("june_recalculation"));
						retention.setDecemberRecalculation(rs.getBoolean("december_recalculation"));
						retention.setVigencyDate(rs.getDate("vigency_date"));
						retention.setEnable(rs.getBoolean("enable"));


						return retention;

					}
				});

		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	@Override
	public List<RetentionModel> loadAllRetentionByEnable(Boolean enable) {
		String sql = "SELECT * FROM retention WHERE june_recalculation=false and december_recalculation= false and enable = "+ enable;
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<RetentionModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			
			RetentionModel retention = new RetentionModel();
			
			retention.setId((Integer)row.get("id"));
			retention.setStrench((Integer) row.get("strench"));
			retention.setFrom((double) row.get("from"));
			retention.setTo((double) row.get("to"));
			retention.setPorcentApply((double) row.get("porcent_apply"));
			retention.setOverExcess((double) row.get("over_Excess"));
			retention.setFixedFee((double) row.get("fixed_fee"));
			retention.setJuneRecalculation((Boolean) row.get("june_recalculation"));
			retention.setDecemberRecalculation((Boolean) row.get("december_recalculation"));
			retention.setVigencyDate((Date) row.get("vigency_date"));
			retention.setEnable((Boolean) row.get("enable"));
			
			

			result.add(retention);
		}

		return result;
	}

}
