package com.htc.payroll.payrollservice.service;

import java.util.List;

import com.htc.payroll.payrollservice.exception.EmptyException;
import com.htc.payroll.payrollservice.exception.NullObjectException;
import com.htc.payroll.payrollservice.model.RetentionModel;

public interface RetentionService {
	
	public RetentionModel findEnableRetention(Boolean enable) throws NullObjectException;
	public List<RetentionModel> loadAllEnableRetention(Boolean enable) throws EmptyException;

}
