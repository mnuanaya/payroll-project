package com.htc.payroll.payrollservice.exception;

public class NullObjectException extends Exception {

	private static final long serialVersionUID = 1L;

	private final long code;
	private final String description;

	public NullObjectException(Exception ex) {
		super(ex);
		this.code = 780;
		description = "NullObjectException";
	}

	public NullObjectException(String description, Exception ex) {
		super(ex);
		this.code = 780;
		this.description = description;
	}

	public NullObjectException(long code, String description, Exception ex) {
		super(ex);
		this.code = code;
		this.description = description;
	}

	public NullObjectException(long code, String description) {
		this.code = code;
		this.description = description;
	}

	public NullObjectException(String description) {
		this.code = 780;
		this.description = description;
	}

	@Override
	public String getMessage() {
		return description;
	}

	public long getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApiServiceException [code=").append(code).append(", description=").append(description)
				.append("]");
		return builder.toString();
	}
}
