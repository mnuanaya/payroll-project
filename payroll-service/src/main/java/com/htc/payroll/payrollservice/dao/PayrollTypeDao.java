package com.htc.payroll.payrollservice.dao;

import java.util.List;

import com.htc.payroll.payrollservice.model.PayrollTypeModel;

public interface PayrollTypeDao {
	
	public List<PayrollTypeModel> loadAllPayrollType();

	public PayrollTypeModel findPayrollTypeById(long id);

}
