package com.htc.payroll.payrollservice.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="discounts")
public class DiscountsModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name= "id")
	private Integer id;
	@Column(name= "isss")
	private double isss;
	
	@Column(name= "afp")
	private double afp;
	
	@Column(name= "isr")
	private double isr;
	
	@Column(name= "vigency_date")
	private Date vigencyDate;
	
	@Column(name= "enable")
	private boolean enable;
	
	public DiscountsModel() {
		
	}
	

	
	public DiscountsModel(Integer id, double isss, double afp, double isr, Date vigencyDate, boolean enable) {
		super();
		this.id = id;
		this.isss = isss;
		this.afp = afp;
		this.isr = isr;
		this.vigencyDate = vigencyDate;
		this.enable = enable;
	}



	public DiscountsModel(double isss, double afp, double isr, Date vigencyDate, boolean enable) {
		super();
		this.isss = isss;
		this.afp = afp;
		this.isr = isr;
		this.vigencyDate = vigencyDate;
		this.enable = enable;
	}



	public Integer getId() {
		return id;
	}


	public double getIsss() {
		return isss;
	}
	public double getAfp() {
		return afp;
	}
	public double getIsr() {
		return isr;
	}
	
	
	public void setId(Integer id) {
		this.id = id;
	}



	public void setIsss(double isss) {
		this.isss = isss;
	}



	public void setAfp(double afp) {
		this.afp = afp;
	}



	public void setIsr(double isr) {
		this.isr = isr;
	}
	
	

	public Date getVigencyDate() {
		return vigencyDate;
	}



	public void setVigencyDate(Date vigencyDate) {
		this.vigencyDate = vigencyDate;
	}



	public boolean isEnable() {
		return enable;
	}



	public void setEnable(boolean enable) {
		this.enable = enable;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DiscountsModel [id=");
		builder.append(id);
		builder.append(", isss=");
		builder.append(isss);
		builder.append(", afp=");
		builder.append(afp);
		builder.append(", isr=");
		builder.append(isr);
		builder.append(", vigencyDate=");
		builder.append(vigencyDate);
		builder.append(", enable=");
		builder.append(enable);
		builder.append("]");
		return builder.toString();
	}



	
	
	
	
}
