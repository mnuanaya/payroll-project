package com.htc.payroll.payrollservice.service.impl;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.core.IsSame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.htc.payroll.payrollservice.dao.CompanyDao;
import com.htc.payroll.payrollservice.dao.ContractTypeDao;
import com.htc.payroll.payrollservice.dao.DiscountsDao;
import com.htc.payroll.payrollservice.dao.EmployeeDao;
import com.htc.payroll.payrollservice.dao.PayrollDao;
import com.htc.payroll.payrollservice.dao.PayrollDetailDao;
import com.htc.payroll.payrollservice.dao.PayrollTypeDao;
import com.htc.payroll.payrollservice.dao.RetentionDao;
import com.htc.payroll.payrollservice.exception.EmptyException;
import com.htc.payroll.payrollservice.exception.NullObjectException;
import com.htc.payroll.payrollservice.exception.OperationException;
import com.htc.payroll.payrollservice.model.CompanyModel;
import com.htc.payroll.payrollservice.model.ContractTypeModel;
import com.htc.payroll.payrollservice.model.DiscountsModel;
import com.htc.payroll.payrollservice.model.EmployeeModel;
import com.htc.payroll.payrollservice.model.PayrollDetailModel;
import com.htc.payroll.payrollservice.model.PayrollModel;
import com.htc.payroll.payrollservice.model.PayrollTypeModel;
import com.htc.payroll.payrollservice.model.RetentionModel;
import com.htc.payroll.payrollservice.repository.DiscountsRepository;
import com.htc.payroll.payrollservice.service.CompanyService;
import com.htc.payroll.payrollservice.service.DiscountService;
import com.htc.payroll.payrollservice.service.PayrollService;
import com.htc.payroll.payrollservice.service.RetentionService;
import com.htc.payroll.payrollservice.util.Constants;

@Component
public class PayrollServiceImpl implements PayrollService {

	private static final Logger log = LoggerFactory.getLogger(PayrollServiceImpl.class);

	@Autowired
	public PayrollDao payrollDao;

	@Autowired
	public CompanyService companyService;

	@Autowired
	public CompanyDao companyDao;

	@Autowired
	public PayrollDetailDao payrollDetailDao;

	@Autowired
	public EmployeeDao employeeDao;

	@Autowired
	public PayrollTypeDao payrollTypeDao;
	
	@Autowired
	public ContractTypeDao contractTypeDao;
	
	
	@Autowired
	public DiscountService discountService;

	@Autowired
	public RetentionService retentionService;
	
	@Autowired
	DiscountsRepository discountRepository;

	@Autowired
	private Environment env;

	@Transactional
	@Override
	public boolean insert(PayrollModel payroll) throws OperationException, NullObjectException, EmptyException {

		try {

			if (payroll == null || payroll.getDetails() == null || payroll.getDetails().isEmpty()) {
				log.error("No se puede insertar planilla. campos vacios");
				throw new OperationException(Long.valueOf(env.getProperty(Constants.PAYROLL_NULL_CODE)),
						env.getProperty(Constants.MESSAGE_PAYROLL_NULL));

			} else if (companyService.getCompanyById(payroll.getIdCompany()) == null) {
				log.error("Id company no encontrado");
				throw new NullObjectException(Long.valueOf(env.getProperty(Constants.COMPANY_NULL_CODE)),
						env.getProperty(Constants.MESSAGE_COMPANY_NULL));

			}

			Double nominalSalary = 0.00;
			Double salaryNominal=0.00;
			Double totalSalary = 0.00;
			Double otherIncome = 0.00;
			Double otherDi = 0.00;
			Double isss = 0.00;
			Double afp = 0.00;
			Double totalIncome = 0.00;
			Double totalDiscount = 0.00;
			Double isr = 0.00;
			
			
			Date fechas = null;
			Integer idCompany=null;

			List<EmployeeModel> employee = new ArrayList<>();
			List<CompanyModel> company = new ArrayList<>();
			List<ContractTypeModel> listType = new ArrayList<>();
			
			//PARA VALIDAR FECHAS
			List<PayrollModel> fechaValida = new ArrayList<>();
			int mes = 0;
			long a�o = 0;
			Date payDate;
			
			
			Double tisr=0.00;
			Double tAfp=0.00;
			Double tIsss=0.00;
			List<Date> fechaPay = new ArrayList<>();
			for(DiscountsModel dModel : discountService.loadAllDiscountByEnable(true)) {
				tisr=dModel.getIsr();
				tAfp = dModel.getAfp();
				tIsss = dModel.getIsss();
				
			}
			
			Integer tStrech=0;
			Double tFrom=0.00;
			Double tTo=0.00;
			Double tporcentR=0.00;
			Double tOverRet = 0.00;
			Double tFixedRet = 0.00;
			Boolean tJuneRec = false;
			Boolean tDeceRec = false;
			for(RetentionModel rModel : retentionService.loadAllEnableRetention(true)) {
				
				tStrech= rModel.getStrench();
				tFrom = rModel.getFrom();
				tTo = rModel.getTo();
				tporcentR = rModel.getPorcentApply();
				tOverRet = rModel.getOverExcess();
				tFixedRet = rModel.getFixedFee();
				
			}

			for (PayrollDetailModel d : payroll.getDetails()) {
				
				EmployeeModel empl = employeeDao.findEmployeeById(d.getIdEmployee());
				
				if(empl==null) {
					log.error("Empleado No existe");
					throw new OperationException(Long.valueOf(env.getProperty(Constants.EMPLEADO_NULL_CODE)),
							env.getProperty(Constants.MESSAGE_EMPLEADO_NULL));
				}
				
				CompanyModel compa = companyDao.findCompanyById(d.getIdCompany());
				
				if(compa == null) {
					log.error("Empresa No existe");
					throw new OperationException(Long.valueOf(env.getProperty(Constants.COMPANY_NULL_CODE)),
							env.getProperty(Constants.MESSAGE_COMPANY_NULL));
				}
				idCompany = compa.getId();
				
				ContractTypeModel payType = contractTypeDao.findContractById(empl.getIdContractType());
				if(payType == null) {
					log.error("Tipo Contrato No existe");
					throw new OperationException(Long.valueOf(env.getProperty(Constants.CONTRACT_TYPE_NOT_FOUND_CODE)),
							env.getProperty(Constants.MESSAGE_CONTRACT_NOT_FOUND));
				}
				
				
				//validando fechas que no esten en la base de datos
				
				
				payDate = d.getPayrollDate();
				String text = payDate.toString();
				System.out.println(text+ "llego a metodo insert");
				
				///////validaciones de fecha arriba y verificacion de calculos y traer desde base de datos constantes
				
				//fechaValida = payrollDao.loadPayrollDate(mes, a�o);
				
				if(d.getNominalSalary()<0) {
					throw new OperationException(Long.valueOf(env.getProperty(Constants.POSITIVE_CODE)),
							env.getProperty(Constants.MESSAGE_POSITIVE));
				}else if (d.getOtherIncome()<0) {
					throw new OperationException(Long.valueOf(env.getProperty(Constants.POSITIVE_OTHER_INCOME_CODE)),
							env.getProperty(Constants.MESSAGE_POSITIVE_OTHER_INCOME));
				}else if (d.getOtherDiscount()<0) {
					throw new OperationException(Long.valueOf(env.getProperty(Constants.POSITIVE_OTHER_DISCOUNT_CODE)),
							env.getProperty(Constants.MESSAGE_POSITIVE_OTHER_DISCOUNT));
				}
				
				if(empl.getIdContractType()==1) {
					
					
					
					otherIncome = d.getOtherIncome();
					salaryNominal += d.getNominalSalary();
					nominalSalary = d.getNominalSalary() + otherIncome;
					
					//d.setAfp((d.getNominalSalary() + d.getOtherIncome()) * 0.0725);
					d.setAfp((d.getNominalSalary() + d.getOtherIncome()) * tAfp);
					
					afp += d.getAfp();
					
					
					//isss
					if(nominalSalary<=1000) {
						//d.setIsss((d.getNominalSalary() + d.getOtherIncome()) * 0.03);
						d.setIsss((d.getNominalSalary() + d.getOtherIncome()) * tIsss);
						isss += d.getIsss();
						
					}else {
						d.setIsss(30.00);
						isss += d.getIsss();
						
					}
					
					d.setTotalIncome((d.getNominalSalary() + d.getOtherIncome()) - (d.getIsss() + d.getAfp()));
					
					totalIncome = d.getTotalIncome();
					
					///isr
					if(totalIncome <= 0.01 || totalIncome <= 472.00) {
						
						d.setIsr(0.00);
						isr += d.getIsr() ;
						
					}else if (totalIncome <= 472.01 || totalIncome <= 895.24) {
						
						
						d.setIsr((((d.getNominalSalary() + d.getOtherIncome()) - (d.getIsss() + d.getAfp()) - 472.00) * 0.10) + 17.67);
						isr += d.getIsr();
						
					}else if (totalIncome <= 895.25 || totalIncome <= 2038.10) {
						
						d.setIsr((((d.getNominalSalary() + d.getOtherIncome()) - (d.getIsss() + d.getAfp()) - 895.24) * 0.20) + 60.00);
						isr += d.getIsr();
						
					}else if (totalIncome <= 2038.11 || totalIncome > 2038.11) {
						
						d.setIsr((((d.getNominalSalary() + d.getOtherIncome()) - (d.getIsss() + d.getAfp()) - 2038.10) * 0.30) + 288.57);
						isr += d.getIsr();
						
					}
						
					
					//total discount
					otherDi += d.getOtherDiscount();
					
					d.setTotalDiscount(d.getIsss() + d.getAfp() + d.getIsr() + d.getOtherDiscount());
					totalDiscount += d.getTotalDiscount();
					
					
					//total income
					totalIncome=0.00;
					d.setTotalIncome((d.getNominalSalary() + d.getOtherIncome()) - (d.getIsss() + d.getAfp() + d.getIsr() + d.getOtherDiscount()));
					totalIncome += d.getTotalIncome();
					
					
					//total salary
					d.setTotalSalary((d.getNominalSalary() + d.getOtherIncome()) - (d.getIsss() + d.getAfp() + d.getIsr() + d.getOtherDiscount()));
					totalSalary += d.getTotalSalary();
					
					
				}else if(empl.getIdContractType()==2){
					//servicio profesional renta se calcula solo como el 10%
					otherIncome = d.getOtherIncome();
					salaryNominal += d.getNominalSalary();
					nominalSalary = d.getNominalSalary() + otherIncome;
					
					d.setAfp(0.00);
					afp += d.getAfp();
					
					d.setIsss(0.00);
					isss += d.getIsss();
					
					
					//isr
					//d.setIsr(((d.getNominalSalary() + d.getOtherIncome()) * 0.10));
					d.setIsr(((d.getNominalSalary() + d.getOtherIncome()) * tisr));
					isr += d.getIsr();
					
					
					//total discount
					otherDi += d.getOtherDiscount();
					
					d.setTotalDiscount(d.getIsss() + d.getAfp() + d.getIsr() + d.getOtherDiscount());
					totalDiscount += d.getTotalDiscount();
					
					
					//total income
					d.setTotalIncome((d.getNominalSalary() + d.getOtherIncome()) - (d.getIsss() + d.getAfp() + d.getIsr() + d.getOtherDiscount()));
					totalIncome += d.getTotalIncome();
					
					//total salary
					d.setTotalSalary((d.getNominalSalary() + d.getOtherIncome()) - (d.getIsss() + d.getAfp() + d.getIsr() + d.getOtherDiscount()));
					totalSalary += d.getTotalSalary();
					
					 
					
					
				}else if(empl.getIdContractType()==null){
					
					throw new OperationException(Long.valueOf(env.getProperty(Constants.TIPO_PAY_NULL_CODE)),
							env.getProperty(Constants.MESSAGE_TIPO_PAY_NULL));
					
				}else {
					throw new OperationException(Long.valueOf(env.getProperty(Constants.TIPO_PAY_NOT_FOUND_CODE)),
						env.getProperty(Constants.MESSAGE_NOT_FOUND_PAYROLL));
				}
				
//				nominalSalary = d.getNominalSalary();
//				otherDi += d.getOtherDiscount();
//				otherIncome += d.getOtherIncome();
//
//				// subtotalSalary += d.getTotalSalary();
//				totalSalary += d.getNominalSalary();
				fechas = d.getPayrollDate();
				fechaPay.add(d.getPayrollDate());
				listType.add(payType);
				company.add(compa);
				employee.add(empl);
			}

			
			payroll.setPayrollDate(fechas);
			payroll.setSalary(salaryNominal);
			payroll.setOtherIncome(otherIncome);
			payroll.setSubtotalSalary(salaryNominal + otherIncome);
			payroll.setIsss(isss);
			payroll.setAfp(afp);
			payroll.setIsr(isr);
			payroll.setOtherDiscount(otherDi);
			payroll.setTotalSalary(totalSalary);
			payroll.setTotalIncome(totalIncome);
			payroll.setIdCompany(idCompany);

			Integer llave = payrollDao.insertAndReturn(payroll);

			return payrollDetailDao.inserBatch(payroll.getDetails(), llave);

		} catch (OperationException e) {
			log.error(e.getMessage());
			throw e;

		} catch (NullObjectException e) {
			log.error(e.getMessage());
			throw e;
		} catch (Exception e) {

			log.error(e.getMessage());
			throw e;
		}

	}

	@Override
	public Integer insertAndReturn(PayrollModel payroll) throws OperationException {

		return null;
	}

	@Override
	public boolean inserBatchPayroll(List<PayrollModel> payrolls) throws OperationException {

		return false;
	}

	@Override
	public List<PayrollModel> loadAllPayroll() throws EmptyException {

		return null;
	}

	@Override
	public PayrollModel getPayrollById(long fid) throws NullObjectException {

		return null;
	}

	@Override
	public boolean updatePayroll(Integer id, PayrollModel payroll) throws OperationException {

		return false;
	}

	@Override
	public List<PayrollModel> loadPayrollDate(String mes, long a�o) {
		List<PayrollModel> list = payrollDao.loadPayrollDate(mes, a�o);
		
		return list;
	}

}
