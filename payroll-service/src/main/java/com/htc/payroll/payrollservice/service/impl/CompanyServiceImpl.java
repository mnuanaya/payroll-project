package com.htc.payroll.payrollservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.htc.payroll.payrollservice.dao.CompanyDao;
import com.htc.payroll.payrollservice.exception.NullObjectException;
import com.htc.payroll.payrollservice.model.CompanyModel;
import com.htc.payroll.payrollservice.service.CompanyService;

@Component
public class CompanyServiceImpl implements CompanyService {
	@Autowired
	CompanyDao companyDao;

	@Override
	public CompanyModel getCompanyById(long cusid) throws NullObjectException {

		CompanyModel companyModel = companyDao.findCompanyById(cusid);
		return companyModel;
	}

}
