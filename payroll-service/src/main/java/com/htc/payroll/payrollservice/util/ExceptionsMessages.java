package com.htc.payroll.payrollservice.util;

import java.util.Date;
import java.util.List;

public class ExceptionsMessages  {

	private String message;
	private String description;
	private Date timestamp;
	private List<String> errors;

	public ExceptionsMessages(String message, String description) {
		this.timestamp=new Date();
		this.message = message;
		this.description = description;
	}

	public ExceptionsMessages(String message, String description, List<String> errors) {
		super();
		this.timestamp=new Date();
		this.message = message;
		this.description = description;
		this.errors = errors;
	}

	public String getMessage() {
		return message;
	}
	public String getDescription() {
		return description;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public List<String> getErrors() {
		return errors;
	}


	/**
	 * 
	 */
//	private static final long serialVersionUID = 1L;
//	private final String code;
//	private final String description;
//	
//	public ExceptionsMessages(Exception ex) {
//		super(ex);
//		this.code = Constants.CODE_NOT_FOUND;
//		this.description = Constants.ERROR_NOT_FOOUND_DESCRIPTION;
//	}
//	
//	public ExceptionsMessages(String code, String description) {
//		super();
//		this.code = code;
//		this.description = description;
//	}
//	
//	public ExceptionsMessages(String code, String description, Exception ex) {
//		super();
//		this.code = code;
//		this.description = description;
//	}
//
//	public String getCode() {
//		return code;
//	}
//
//	public String getDescription() {
//		return description;
//	}
//
//	@Override
//	public String toString() {
//		return "ExceptionsMessages [code=" + code + ", description=" + description + "]";
//	}

}
