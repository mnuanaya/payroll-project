package com.htc.payroll.payrollservice.model;

import java.io.Serializable;
import java.sql.Date;

public class EmployeeModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String names;
	private Date bornDate;
	private String isssNumber;
	private String afpNumber;
	private String position;
	private Date incorporationDate;
	private Integer idContractType;
	
	public EmployeeModel() {
		
	}

	public EmployeeModel(String names, Date bornDate, String isssNumber, String afpNumber, String position,
			Date incorporationDate, Integer idContractType) {
		super();
		this.names = names;
		this.bornDate = bornDate;
		this.isssNumber = isssNumber;
		this.afpNumber = afpNumber;
		this.position = position;
		this.incorporationDate = incorporationDate;
		this.idContractType = idContractType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public Date getBornDate() {
		return bornDate;
	}

	public void setBornDate(Date bornDate) {
		this.bornDate = bornDate;
	}

	public String getIsssNumber() {
		return isssNumber;
	}

	public void setIsssNumber(String isssNumber) {
		this.isssNumber = isssNumber;
	}

	public String getAfpNumber() {
		return afpNumber;
	}

	public void setAfpNumber(String afpNumber) {
		this.afpNumber = afpNumber;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Date getIncorporationDate() {
		return incorporationDate;
	}

	public void setIncorporationDate(Date incorporationDate) {
		this.incorporationDate = incorporationDate;
	}

	public Integer getIdContractType() {
		return idContractType;
	}

	public void setIdContractType(Integer idContractType) {
		this.idContractType = idContractType;
	}

	@Override
	public String toString() {
		return "EmployeeModel [id=" + id + ", names=" + names + ", bornDate=" + bornDate + ", isssNumber=" + isssNumber
				+ ", afpNumber=" + afpNumber + ", position=" + position + ", incorporationDate=" + incorporationDate
				+ ", idContractType=" + idContractType + "]";
	}
	
	

	
	

}
