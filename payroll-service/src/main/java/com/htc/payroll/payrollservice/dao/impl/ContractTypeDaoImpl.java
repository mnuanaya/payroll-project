package com.htc.payroll.payrollservice.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.htc.payroll.payrollservice.dao.ContractTypeDao;
import com.htc.payroll.payrollservice.model.CompanyModel;
import com.htc.payroll.payrollservice.model.ContractTypeModel;

@Repository
public class ContractTypeDaoImpl extends JdbcDaoSupport implements ContractTypeDao {
	
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<ContractTypeModel> loadAllContract() {
		
		String sql = "SELECT * FROM contract_type";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<ContractTypeModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			ContractTypeModel contract = new ContractTypeModel();
			
			
			contract.setId((Integer) row.get("id"));
			contract.setType((String) row.get("type"));
			

			result.add(contract);
		}

		return result;
	}

	@Override
	public ContractTypeModel findContractById(long id) {
		
		String sql = "SELECT * FROM contract_type WHERE id = ?";
		List<ContractTypeModel> list = getJdbcTemplate().query(sql, new Object[] { id }, new RowMapper<ContractTypeModel>() {
			@Override
			public ContractTypeModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				ContractTypeModel contract = new ContractTypeModel();
				
				contract.setId(rs.getInt("id"));
				contract.setType(rs.getString("type"));
				
				

				return contract;
			}
		});
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

}
