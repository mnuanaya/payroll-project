package com.htc.payroll.payrollservice.dao;

import java.sql.Date;
import java.util.List;

import com.htc.payroll.payrollservice.model.DiscountsModel;

public interface DiscountsDao {
	
	public DiscountsModel findDiscountByEnable(Boolean enable);
	public List<DiscountsModel> loadAllDiscountByEnbale(Boolean enable);
	
}
