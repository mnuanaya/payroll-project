package com.htc.payroll.payrollservice.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ResponseHttpExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		ExceptionsMessages exceptionResponse = new ExceptionsMessages( ex.getMessage(),
				request.getDescription(false));
		return new ResponseEntity<>(exceptionResponse,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
		HttpHeaders headers, HttpStatus status, WebRequest request) {
		HttpHeaders header = new HttpHeaders();
		List<String> errors = new ArrayList<>();
		
		for (Object error : ex.getBindingResult().getAllErrors()) {
			if(error instanceof FieldError) {
		        FieldError fieldError = (FieldError) error;
		        errors.add("variable: "+fieldError.getField()+" , message: "+fieldError.getDefaultMessage());
		        header.add("Error: "+fieldError.getField()," , message: "+fieldError.getDefaultMessage());
			}
		}
		ExceptionsMessages exceptionResponse = new ExceptionsMessages("validation Failed","Variable(s) no valid",errors);
		return new ResponseEntity<>(header, HttpStatus.BAD_REQUEST);
	}
}
