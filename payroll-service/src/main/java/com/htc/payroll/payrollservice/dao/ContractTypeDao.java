package com.htc.payroll.payrollservice.dao;

import java.util.List;

import com.htc.payroll.payrollservice.model.ContractTypeModel;

public interface ContractTypeDao {
	
	public List<ContractTypeModel> loadAllContract();

	public ContractTypeModel findContractById(long id);

}
