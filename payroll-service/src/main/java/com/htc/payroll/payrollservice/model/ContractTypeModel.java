package com.htc.payroll.payrollservice.model;

import java.io.Serializable;

public class ContractTypeModel implements Serializable {


	private static final long serialVersionUID = 1L;
	
	Integer id;
	String type;

	public ContractTypeModel() {
		
	}
	
	

	public ContractTypeModel(Integer id, String type) {
		super();
		this.id = id;
		this.type = type;
	}



	public ContractTypeModel(String type) {
		super();
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
	

	public Integer getId() {
		return id;
	}

	

	public void setId(Integer id) {
		this.id = id;
	}



	public void setType(String type) {
		this.type = type;
	}



	@Override
	public String toString() {
		return "ContractTypeModel [id=" + id + ", type=" + type + "]";
	}
	
	

}
