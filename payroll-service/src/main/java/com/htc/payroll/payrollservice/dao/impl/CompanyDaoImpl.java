package com.htc.payroll.payrollservice.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.htc.payroll.payrollservice.dao.CompanyDao;
import com.htc.payroll.payrollservice.model.CompanyModel;

@Repository
public class CompanyDaoImpl extends JdbcDaoSupport implements CompanyDao {
	
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<CompanyModel> loadAllCompanies() {
		
		String sql = "SELECT * FROM company";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<CompanyModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			CompanyModel company = new CompanyModel();
			
			company.setId((Integer) row.get("id"));
			company.setName((String) row.get("name"));
			company.setNit((String) row.get("nit"));
			company.setPhoneNumber((String) row.get("phone_number"));
			company.setSector((String) row.get("sector"));

			result.add(company);
		}

		return result;
	}

	@Override
	public CompanyModel findCompanyById(long id) {
		String sql = "SELECT * FROM company WHERE id = ?";
		List<CompanyModel> list = getJdbcTemplate().query(sql, new Object[] { id }, new RowMapper<CompanyModel>() {
			@Override
			public CompanyModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				CompanyModel company = new CompanyModel();
				
				company.setId(rs.getInt("id"));
				company.setName(rs.getString("name"));
				company.setNit(rs.getString("nit"));
				company.setPhoneNumber(rs.getString("phone_number"));
				company.setSector(rs.getString("sector"));
				

				return company;
			}
		});
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

}
