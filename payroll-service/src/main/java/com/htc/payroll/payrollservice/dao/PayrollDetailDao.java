package com.htc.payroll.payrollservice.dao;

import java.util.List;

import com.htc.payroll.payrollservice.model.PayrollDetailModel;

public interface PayrollDetailDao {
	
//	public boolean insert(PayrollDetailModel detail);
//
	public Integer insertAndReturn(PayrollDetailModel detail);
//
//	public boolean inserBatch(List<PayrollDetailModel> details);
//
	public boolean inserBatch(List<PayrollDetailModel> details, Integer idPayroll);
//
//	public List<PayrollDetailModel> loadAllDetail();
//
//	public PayrollDetailModel findDetailById(Integer detid);
//
	public List<PayrollDetailModel> DetailById(Integer detid);
//
//	public boolean updateDetail(Integer id, PayrollDetailModel detail);
//
//	public boolean deleteDetail(PayrollDetailModel id);
//	
	public List<PayrollDetailModel> loadAllDetailInPayroll(Integer id);

}
