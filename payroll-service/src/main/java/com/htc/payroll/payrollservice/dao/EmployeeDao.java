package com.htc.payroll.payrollservice.dao;

import java.util.List;

import com.htc.payroll.payrollservice.model.EmployeeModel;

public interface EmployeeDao {

	public List<EmployeeModel> loadAllEmployee();

	public EmployeeModel findEmployeeById(long id);
}
