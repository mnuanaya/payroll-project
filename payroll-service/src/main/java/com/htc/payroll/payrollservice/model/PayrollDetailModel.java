package com.htc.payroll.payrollservice.model;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import antlr.collections.List;

public class PayrollDetailModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer idPayroll;
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone="America/El_Salvador")
	private Date payrollDate;
	private Integer idPayrollType;
	private double nominalSalary;
	private double otherIncome;
	private double otherDiscount;
	private double isss;
	private double afp;
	private double isr;
	private String vigencyDiscounts;
	private String vigencyRetention;
	private Integer idEmployee;
	private Integer idCompany;
	private double totalIncome;
	private double totalDiscount;
	private double totalSalary;
	
	
	public PayrollDetailModel() {
		
	}


	public PayrollDetailModel(Integer id, Integer idPayroll, Date payrollDate, Integer idPayrollType,
			double nominalSalary, double otherIncome, double otherDiscount, double isss, double afp, double isr,
			String vigencyDiscounts, String vigencyRetention, Integer idEmployee, Integer idCompany, double totalIncome,
			double totalDiscount, double totalSalary) {
		super();
		this.id = id;
		this.idPayroll = idPayroll;
		this.payrollDate = payrollDate;
		this.idPayrollType = idPayrollType;
		this.nominalSalary = nominalSalary;
		this.otherIncome = otherIncome;
		this.otherDiscount = otherDiscount;
		this.isss = isss;
		this.afp = afp;
		this.isr = isr;
		this.vigencyDiscounts = vigencyDiscounts;
		this.vigencyRetention = vigencyRetention;
		this.idEmployee = idEmployee;
		this.idCompany = idCompany;
		this.totalIncome = totalIncome;
		this.totalDiscount = totalDiscount;
		this.totalSalary = totalSalary;
	}


	public PayrollDetailModel(Integer idPayroll, Date payrollDate, Integer idPayrollType, double nominalSalary,
			double otherIncome, double otherDiscount, double isss, double afp, double isr, String vigencyDiscounts,
			String vigencyRetention, Integer idEmployee, Integer idCompany, double totalIncome, double totalDiscount,
			double totalSalary) {
		super();
		this.idPayroll = idPayroll;
		this.payrollDate = payrollDate;
		this.idPayrollType = idPayrollType;
		this.nominalSalary = nominalSalary;
		this.otherIncome = otherIncome;
		this.otherDiscount = otherDiscount;
		this.isss = isss;
		this.afp = afp;
		this.isr = isr;
		this.vigencyDiscounts = vigencyDiscounts;
		this.vigencyRetention = vigencyRetention;
		this.idEmployee = idEmployee;
		this.idCompany = idCompany;
		this.totalIncome = totalIncome;
		this.totalDiscount = totalDiscount;
		this.totalSalary = totalSalary;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getIdPayroll() {
		return idPayroll;
	}


	public void setIdPayroll(Integer idPayroll) {
		this.idPayroll = idPayroll;
	}


	public Date getPayrollDate() {
		return payrollDate;
	}


	public void setPayrollDate(Date payrollDate) {
		this.payrollDate = payrollDate;
	}


	public Integer getIdPayrollType() {
		return idPayrollType;
	}


	public void setIdPayrollType(Integer idPayrollType) {
		this.idPayrollType = idPayrollType;
	}


	public double getNominalSalary() {
		return nominalSalary;
	}


	public void setNominalSalary(double nominalSalary) {
		this.nominalSalary = nominalSalary;
	}


	public double getOtherIncome() {
		return otherIncome;
	}


	public void setOtherIncome(double otherIncome) {
		this.otherIncome = otherIncome;
	}


	public double getOtherDiscount() {
		return otherDiscount;
	}


	public void setOtherDiscount(double otherDiscount) {
		this.otherDiscount = otherDiscount;
	}


	public double getIsss() {
		return isss;
	}


	public void setIsss(double isss) {
		this.isss = isss;
	}


	public double getAfp() {
		return afp;
	}


	public void setAfp(double afp) {
		this.afp = afp;
	}


	public double getIsr() {
		return isr;
	}


	public void setIsr(double isr) {
		this.isr = isr;
	}


	public String getVigencyDiscounts() {
		return vigencyDiscounts;
	}


	public void setVigencyDiscounts(String vigencyDiscounts) {
		this.vigencyDiscounts = vigencyDiscounts;
	}


	public String getVigencyRetention() {
		return vigencyRetention;
	}


	public void setVigencyRetention(String vigencyRetention) {
		this.vigencyRetention = vigencyRetention;
	}


	public Integer getIdEmployee() {
		return idEmployee;
	}


	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}


	public Integer getIdCompany() {
		return idCompany;
	}


	public void setIdCompany(Integer idCompany) {
		this.idCompany = idCompany;
	}


	public double getTotalIncome() {
		return totalIncome;
	}


	public void setTotalIncome(double totalIncome) {
		this.totalIncome = totalIncome;
	}


	public double getTotalDiscount() {
		return totalDiscount;
	}


	public void setTotalDiscount(double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}


	public double getTotalSalary() {
		return totalSalary;
	}


	public void setTotalSalary(double totalSalary) {
		this.totalSalary = totalSalary;
	}


	@Override
	public String toString() {
		return "PayrollDetailModel [id=" + id + ", idPayroll=" + idPayroll + ", payrollDate=" + payrollDate
				+ ", idPayrollType=" + idPayrollType + ", nominalSalary=" + nominalSalary + ", otherIncome="
				+ otherIncome + ", otherDiscount=" + otherDiscount + ", isss=" + isss + ", afp=" + afp + ", isr=" + isr
				+ ", vigencyDiscounts=" + vigencyDiscounts + ", vigencyRetention=" + vigencyRetention + ", idEmployee="
				+ idEmployee + ", idCompany=" + idCompany + ", totalIncome=" + totalIncome + ", totalDiscount="
				+ totalDiscount + ", totalSalary=" + totalSalary + "]";
	}


	
	

}
