package com.htc.payroll.payrollservice.service;

import com.htc.payroll.payrollservice.exception.NullObjectException;
import com.htc.payroll.payrollservice.model.CompanyModel;

public interface CompanyService {
	
	public CompanyModel getCompanyById(long cusid) throws NullObjectException;

}
