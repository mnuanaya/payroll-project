package com.htc.payroll.payrollservice.service;

import java.sql.Date;
import java.util.List;

import com.htc.payroll.payrollservice.exception.EmptyException;
import com.htc.payroll.payrollservice.exception.NullObjectException;
import com.htc.payroll.payrollservice.exception.OperationException;
import com.htc.payroll.payrollservice.model.PayrollDetailModel;

public interface PayrollDetailService {
	
	public boolean insert(PayrollDetailModel detail) throws OperationException;

	public Integer insertAndReturn(PayrollDetailModel detail) throws NullObjectException;

	public boolean inserBatchDetail(List<PayrollDetailModel> details) throws OperationException;

	public List<PayrollDetailModel> loadAllDetails() throws EmptyException;

	public PayrollDetailModel getDetalleById(Integer detail_id) throws NullObjectException;

	public PayrollDetailModel getDetail(Date fechaPay, Integer tipoPay, Integer idCompany, Integer idEmployee, Double salary, Double otherIncome, Double otherDisc) throws NullObjectException;
	
	public PayrollDetailModel getDetail(List<Integer> idp, List<Integer> cantidad) throws NullObjectException;

	//public boolean updateDetail(Integer id, PayrollDetailModel detail) throws OperationException;

	//public boolean deleteDetail(PayrollDetailModel id) throws OperationException;

}
