package com.htc.payroll.payrollservice.dao;

import java.util.List;

import com.htc.payroll.payrollservice.model.CompanyModel;

public interface CompanyDao {
	
	public List<CompanyModel> loadAllCompanies();

	public CompanyModel findCompanyById(long id);

}
