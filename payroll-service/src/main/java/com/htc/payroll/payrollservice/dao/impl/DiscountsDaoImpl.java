package com.htc.payroll.payrollservice.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.htc.payroll.payrollservice.dao.DiscountsDao;
import com.htc.payroll.payrollservice.model.DiscountsModel;
import com.htc.payroll.payrollservice.model.RetentionModel;

@Repository
public class DiscountsDaoImpl extends JdbcDaoSupport implements DiscountsDao {

	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public DiscountsModel findDiscountByEnable(Boolean enable) {
		
		String sql = "SELECT * FROM discounts WHERE enable = ?";
		List<DiscountsModel> list = getJdbcTemplate().query(sql, new Object[] { enable },
				new RowMapper<DiscountsModel>() {

					@Override
					public DiscountsModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
						DiscountsModel discount = new DiscountsModel();
						
						discount.setId(rs.getInt("id"));
						discount.setIsss(rs.getDouble("isss"));
						discount.setAfp(rs.getDouble("afp"));
						discount.setAfp(rs.getDouble("isr"));

						return discount;

					}
				});

		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	@Override
	public List<DiscountsModel> loadAllDiscountByEnbale(Boolean enable) {
		String sql = "SELECT * FROM discounts WHERE enable = "+ enable;
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<DiscountsModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			
			DiscountsModel discount = new DiscountsModel();
			
			discount.setId((Integer) row.get("id"));
			discount.setIsss((Double) row.get("isss"));
			discount.setAfp((Double) row.get("afp"));
			discount.setIsr((Double) row.get("isr"));
			discount.setVigencyDate((Date) row.get("vigency_date"));
			discount.setEnable((Boolean) row.get("enable"));

			
			
			

			result.add(discount);
		}

		return result;
	}

}
