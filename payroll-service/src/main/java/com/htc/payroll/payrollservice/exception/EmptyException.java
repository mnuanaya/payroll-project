package com.htc.payroll.payrollservice.exception;

public class EmptyException extends Exception {

	private static final long serialVersionUID = 1L;

	private final long code;
	private final String description;

	public EmptyException(Exception ex) {
		super(ex);
		this.code = 781;
		description = "EmptyException";
	}

	public EmptyException(String description, Exception ex) {
		super(ex);
		this.code = 781;
		this.description = description;
	}

	public EmptyException(long code, String description, Exception ex) {
		super(ex);
		this.code = code;
		this.description = description;
	}

	public EmptyException(long code, String description) {
		this.code = code;
		this.description = description;
	}

	public EmptyException(String description) {
		this.code = 781;
		this.description = description;
	}

	@Override
	public String getMessage() {
		return description;
	}

	public long getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApiServiceException [code=").append(code).append(", description=").append(description)
				.append("]");
		return builder.toString();
	}
}
