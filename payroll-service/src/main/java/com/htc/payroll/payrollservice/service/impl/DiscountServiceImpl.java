package com.htc.payroll.payrollservice.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.htc.payroll.payrollservice.dao.DiscountsDao;
import com.htc.payroll.payrollservice.exception.EmptyException;
import com.htc.payroll.payrollservice.exception.NullObjectException;
import com.htc.payroll.payrollservice.model.DiscountsModel;
import com.htc.payroll.payrollservice.service.DiscountService;
import com.htc.payroll.payrollservice.util.Constants;

@Service
public class DiscountServiceImpl implements DiscountService {

	@Autowired
	DiscountsDao discountDao;

	@Autowired
	Environment env;

	@Override
	public DiscountsModel findDiscountByEnable(Boolean enable) throws NullObjectException {

		DiscountsModel discountsModel = discountDao.findDiscountByEnable(enable);
		if (discountsModel == null) {

			throw new NullObjectException(Long.valueOf(env.getProperty(Constants.EMPTY_CODE)),
					env.getProperty(Constants.MESSAGE_EMPTY));
		}
		return discountsModel;
	}

	@Override
	public List<DiscountsModel> loadAllDiscountByEnable(Boolean enable) throws EmptyException {
		
		List<DiscountsModel> listDiscount = discountDao.loadAllDiscountByEnbale(enable);

		if (listDiscount.isEmpty()) {
			throw new EmptyException(env.getProperty(Constants.MESSAGE_EMPTY));
		}
		return listDiscount;
	}

}
