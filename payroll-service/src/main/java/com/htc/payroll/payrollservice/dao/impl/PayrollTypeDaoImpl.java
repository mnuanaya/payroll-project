package com.htc.payroll.payrollservice.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.htc.payroll.payrollservice.dao.PayrollTypeDao;
import com.htc.payroll.payrollservice.model.ContractTypeModel;
import com.htc.payroll.payrollservice.model.PayrollTypeModel;

@Repository
public class PayrollTypeDaoImpl extends JdbcDaoSupport implements PayrollTypeDao{
	
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<PayrollTypeModel> loadAllPayrollType() {
		
		String sql = "SELECT * FROM payroll_type";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<PayrollTypeModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			PayrollTypeModel type = new PayrollTypeModel();
			
			
			type.setId((Integer) row.get("id"));
			type.setType((String) row.get("type"));
			

			result.add(type);
		}

		return result;
	}

	@Override
	public PayrollTypeModel findPayrollTypeById(long id) {
		
		String sql = "SELECT * FROM payroll_type WHERE id = ?";
		List<PayrollTypeModel> list = getJdbcTemplate().query(sql, new Object[] { id }, new RowMapper<PayrollTypeModel>() {
			@Override
			public PayrollTypeModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				PayrollTypeModel type = new PayrollTypeModel();
				
				type.setId( rs.getInt("id"));
				type.setType(rs.getString("type"));
				
				

				return type;
			}
		});
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

}
