package com.htc.payroll.payrollservice.service;

import java.util.List;

import com.htc.payroll.payrollservice.exception.EmptyException;
import com.htc.payroll.payrollservice.exception.NullObjectException;
import com.htc.payroll.payrollservice.exception.OperationException;
import com.htc.payroll.payrollservice.model.PayrollModel;

public interface PayrollService {
	
	public boolean insert(PayrollModel payroll) throws OperationException, NullObjectException, EmptyException;

	public Integer insertAndReturn(PayrollModel payroll) throws OperationException;

	public boolean inserBatchPayroll(List<PayrollModel> payrolls) throws OperationException;

	public List<PayrollModel> loadAllPayroll() throws EmptyException;
	
	public List<PayrollModel> loadPayrollDate(String mes, long a�o);

	public PayrollModel getPayrollById(long fid) throws NullObjectException;

	public boolean updatePayroll(Integer id, PayrollModel payroll) throws OperationException;

	//public boolean deleteBill(PayrollModel bill) throws OperationException;

	//public void BillNull(Integer id, PayrollModel billModel) throws NullObjectException;
	
	//public PayrollModel beforeInsertRest(Integer id, List<PayrollDetail> listGet)throws NullObjectException;

}
