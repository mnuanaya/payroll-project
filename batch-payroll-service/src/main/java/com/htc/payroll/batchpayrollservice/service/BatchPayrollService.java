package com.htc.payroll.batchpayrollservice.service;

import java.util.List;

import com.htc.payroll.batchpayrollservice.dto.PayrollDto;
import com.htc.payroll.batchpayrollservice.model.PayrollDetailModel;

public interface BatchPayrollService {

	//public PayrollDto fileList(String filePathpayroll);
	//public List<PayrollDto> fileList(String filePathpayroll);
	public Boolean fileList(String filePathpayroll);
}
