package com.htc.payroll.batchpayrollservice.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.htc.payroll.batchpayrollservice.client.PayrollClient;
import com.htc.payroll.batchpayrollservice.dto.PayrollDto;
import com.htc.payroll.batchpayrollservice.model.PayrollDetailModel;
import com.htc.payroll.batchpayrollservice.model.PayrollModel;
import com.htc.payroll.batchpayrollservice.service.BatchPayrollService;
import com.htc.payroll.batchpayrollservice.util.Constants;

@Service
public class BatchPayrollServiceImpl implements BatchPayrollService{
	
	@Autowired
	private Environment env;
	
	@Autowired
	  Mapper mapper;
	
	@Autowired
	PayrollClient payrollClient;
	
	public static final String SEPARATOR="\\|";
	public static final String QUOTE="\\/";

	@Override
	public Boolean fileList(String filePathpayroll) {

		BufferedReader br = null;
		
		PayrollDto payrollDto = null;
		//PayrollDetailModel detallePay = new PayrollDetailModel();
		
		//PayrollModel payDto = mapper.map(payrollDto, PayrollModel.class);
		
		List<PayrollDetailModel> listaDetalles = new ArrayList<>();
		
	    String[] fields=null;
	    
	    try {
		
		if(filePathpayroll == null || filePathpayroll.isEmpty()) {
			
			List<String> archivos = new ArrayList<>();
			String path = "C://Users/HTC/Desktop/pruebaData";
			String path2 = "C://Users/HTC/Desktop/pruebaData2";
			
			File folder = new File(path);
			File[] listOfFiles = folder.listFiles();
			for (File file : listOfFiles) {
			    if (file.isFile()) {
			        System.out.println(file.getName());
			        archivos.add(file.getName());
				    
			    }
			}
			
			for (String f : archivos) {
				
				 listaDetalles.clear();
				 
			    	File f1 = new File(path+"/"+f);
				    File f2 = new File(path2+"/"+f);
				    
				    br =new BufferedReader(new FileReader("C://Users/HTC/Desktop/pruebaData/"+f));
			         String line = br.readLine();
			         while (null!=line) {
			            fields = line.split(SEPARATOR);
			            System.out.println(Arrays.toString(fields));
			            System.out.println(br.toString());
			            System.out.println("longitud: "+fields.length);
			           
			         if (fields.length<7) {
							System.out.println("ultimo campo vacio");
							listaDetalles.clear();
							line=null;
			            	
			         }else if(fields[0].isEmpty() || fields[1].isEmpty() || fields[2].isEmpty() || fields[3].isEmpty() 
			            		|| fields[4].isEmpty() || fields[5].isEmpty() || fields[6].isEmpty()) {
			            	
			            	System.out.println("archivo con uno o todos los campos vacios");
			            	listaDetalles.clear();
			            	line = null;
					}else {

						payrollDto = new PayrollDto();
						
						PayrollDetailModel detallePay = new PayrollDetailModel();
			             java.util.Date utilDate = parseDate(fields[0]);
			             
			             java.sql.Date sqlDate = new java.sql.Date (utilDate.getTime());
			             
			             detallePay.setPayrollDate(sqlDate);
			             detallePay.setIdPayrollType(Integer.valueOf(fields[1]));
			             detallePay.setIdCompany(Integer.valueOf(fields[2]));
			             detallePay.setIdEmployee(Integer.valueOf(fields[3]));
			             detallePay.setNominalSalary(Double.valueOf(fields[4]));
			             detallePay.setOtherIncome(Double.valueOf(fields[5]));
			             detallePay.setOtherDiscount(Double.valueOf(fields[6]));
			             
				         
			             listaDetalles.add(detallePay);
			             payrollDto.setDetails(listaDetalles);
			             payrollDto.setIdCompany(Integer.valueOf(fields[2]));
				         
				         
			           
			            line = br.readLine();
			           	}
			        }
			
				    System.err.println("Result of move:"+f1.renameTo(f2));
				    payrollClient.insertPayroll(payrollDto);
			}
			
			
			
		}else {
		

		
			listaDetalles.clear();
			br =new BufferedReader(new FileReader("C://Users/HTC/Desktop/pruebaData/"+filePathpayroll+".dat"));
	         String line = br.readLine();
	         while (null!=line) {
	            fields = line.split(SEPARATOR);
	            System.out.println(Arrays.toString(fields));
	            System.out.println(br.toString());
	            System.out.println("longitud: "+fields.length);

	           
	         if (fields.length<7) {
					System.out.println("ultimo campo vacio");
					listaDetalles.clear();
					line=null;
	            	
	         }else if(fields[0].isEmpty() || fields[1].isEmpty() || fields[2].isEmpty() || fields[3].isEmpty() 
	            		|| fields[4].isEmpty() || fields[5].isEmpty() || fields[6].isEmpty()) {
	            	
	            	System.out.println("archivo con uno o todos los campos vacios");
	            	listaDetalles.clear();
	            	line = null;
			}else {
				
				payrollDto = new PayrollDto();
				
				
				PayrollDetailModel detallePay = new PayrollDetailModel();
	             java.util.Date utilDate = parseDate(fields[0]);
	             java.sql.Date sqlDate = new java.sql.Date (utilDate.getTime ());
	             
	             detallePay.setPayrollDate(sqlDate);
	             detallePay.setIdPayrollType(Integer.valueOf(fields[1]));
	             detallePay.setIdCompany(Integer.valueOf(fields[2]));
	             detallePay.setIdEmployee(Integer.valueOf(fields[3]));
	             detallePay.setNominalSalary(Double.valueOf(fields[4]));
	             detallePay.setOtherIncome(Double.valueOf(fields[5]));
	             detallePay.setOtherDiscount(Double.valueOf(fields[6]));
	             
		         
	             listaDetalles.add(detallePay);
	             payrollDto.setDetails(listaDetalles);
	             payrollDto.setIdCompany(Integer.valueOf(fields[2]));
		         
		         
	           
	            line = br.readLine();
	           	}
	        }

	         payrollClient.insertPayroll(payrollDto);

		}

		} catch (Exception e) {

			System.out.println(e.getMessage());
			
		}
		
		return Boolean.TRUE;

	}
	
	private static Date parseDate(String fecha) {
		Date date1=null;
		try {
			date1 = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
		} catch (ParseException e) {
			
			
			e.printStackTrace();
		}  
	    System.out.println(fecha+"\t"+date1); 
	    return date1;

	}

}
