package com.htc.payroll.batchpayrollservice.exception;

public class OperationException extends Exception {
	

	private static final long serialVersionUID = 1L;
	
	private final long code;
	private final String description;
	
	public OperationException(Exception ex) {
		super(ex);
		this.code = 782;	
		description = "OperationException";
	}
	
	public OperationException(String description, Exception ex) {
		super(ex);
		this.code = 782;
		this.description = description;
	}
	
	public OperationException(long code, String description, Exception ex) {
		super(ex);
		this.code = code;
		this.description = description;
	}
	
	public OperationException(long code, String description) {
		this.code = code;
		this.description = description;
	}
	
	public OperationException(String description) {
		this.code = 782;	
		this.description = description;
	}

	@Override
	public String getMessage() {
		return description;
	}
		
	public long getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApiServiceException [code=").append(code).append(", description=").append(description)
				.append("]");
		return builder.toString();
	}	
}
