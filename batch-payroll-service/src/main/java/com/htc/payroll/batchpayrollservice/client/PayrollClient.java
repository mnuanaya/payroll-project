package com.htc.payroll.batchpayrollservice.client;

import javax.validation.Valid;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.htc.payroll.batchpayrollservice.dto.PayrollDto;
import com.htc.payroll.batchpayrollservice.exception.EmptyException;
import com.htc.payroll.batchpayrollservice.exception.NullObjectException;
import com.htc.payroll.batchpayrollservice.exception.OperationException;
import com.htc.payroll.batchpayrollservice.model.PayrollModel;



//@FeignClient(name = "payroll-service/payroll-service")
@FeignClient(name="payroll-service", url="http://localhost:8092/payroll/")
//@FeignClient(name="payroll-service")
@RibbonClient(name="payroll-service")
public interface PayrollClient {
	
	@PostMapping("/")
	public ResponseEntity<Object> insertPayroll(@Valid @RequestBody PayrollDto payrollDto) throws OperationException, NullObjectException,EmptyException;

}
