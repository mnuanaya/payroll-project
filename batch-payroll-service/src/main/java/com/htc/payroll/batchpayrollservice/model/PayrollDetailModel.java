package com.htc.payroll.batchpayrollservice.model;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import antlr.collections.List;

public class PayrollDetailModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone="America/El_Salvador")
	private Date payrollDate;
	private Integer idPayrollType;
	private double nominalSalary;
	private double otherIncome;
	private double otherDiscount;
	private Integer idEmployee;
	private Integer idCompany;

	
	
	public PayrollDetailModel() {
		
	}



	public PayrollDetailModel(Date payrollDate, Integer idPayrollType, double nominalSalary, double otherIncome,
			double otherDiscount, Integer idEmployee, Integer idCompany) {
		super();
		this.payrollDate = payrollDate;
		this.idPayrollType = idPayrollType;
		this.nominalSalary = nominalSalary;
		this.otherIncome = otherIncome;
		this.otherDiscount = otherDiscount;
		this.idEmployee = idEmployee;
		this.idCompany = idCompany;
	}



	public Date getPayrollDate() {
		return payrollDate;
	}



	public void setPayrollDate(Date payrollDate) {
		this.payrollDate = payrollDate;
	}



	public Integer getIdPayrollType() {
		return idPayrollType;
	}



	public void setIdPayrollType(Integer idPayrollType) {
		this.idPayrollType = idPayrollType;
	}



	public double getNominalSalary() {
		return nominalSalary;
	}



	public void setNominalSalary(double nominalSalary) {
		this.nominalSalary = nominalSalary;
	}



	public double getOtherIncome() {
		return otherIncome;
	}



	public void setOtherIncome(double otherIncome) {
		this.otherIncome = otherIncome;
	}



	public double getOtherDiscount() {
		return otherDiscount;
	}



	public void setOtherDiscount(double otherDiscount) {
		this.otherDiscount = otherDiscount;
	}



	public Integer getIdEmployee() {
		return idEmployee;
	}



	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}



	public Integer getIdCompany() {
		return idCompany;
	}



	public void setIdCompany(Integer idCompany) {
		this.idCompany = idCompany;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PayrollDetailModel [payrollDate=");
		builder.append(payrollDate);
		builder.append(", idPayrollType=");
		builder.append(idPayrollType);
		builder.append(", nominalSalary=");
		builder.append(nominalSalary);
		builder.append(", otherIncome=");
		builder.append(otherIncome);
		builder.append(", otherDiscount=");
		builder.append(otherDiscount);
		builder.append(", idEmployee=");
		builder.append(idEmployee);
		builder.append(", idCompany=");
		builder.append(idCompany);
		builder.append("]");
		return builder.toString();
	}


	

}
