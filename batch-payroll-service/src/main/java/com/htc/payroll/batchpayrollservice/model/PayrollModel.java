package com.htc.payroll.batchpayrollservice.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;



public class PayrollModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Date payrollDate;
	private double salary;
	private double otherIncome;
	private double subtotalSalary;
	private double isss;
	private double afp;
	private double isr;
	private double otherDiscount;
	private Integer idCompany;
	private double totalIncome;
	private double totalDiscount;
	private double totalSalary;
	private String observation;
	private boolean enable;
	
	private List<PayrollDetailModel> details;
	
	public PayrollModel() {
		
	}
	
	

	public PayrollModel(List<PayrollDetailModel> details) {
		super();
		this.details = details;
	}



	public PayrollModel(Integer id, Date payrollDate, double salary, double otherIncome, double subtotalSalary,
			double isss, double afp, double isr, double otherDiscount, Integer idCompany, double totalIncome,
			double totalDiscount, double totalSalary, String observation, boolean enable,
			List<PayrollDetailModel> details) {
		super();
		this.id = id;
		this.payrollDate = payrollDate;
		this.salary = salary;
		this.otherIncome = otherIncome;
		this.subtotalSalary = subtotalSalary;
		this.isss = isss;
		this.afp = afp;
		this.isr = isr;
		this.otherDiscount = otherDiscount;
		this.idCompany = idCompany;
		this.totalIncome = totalIncome;
		this.totalDiscount = totalDiscount;
		this.totalSalary = totalSalary;
		this.observation = observation;
		this.enable = enable;
		this.details = details;
	}

	public PayrollModel(Date payrollDate, double salary, double otherIncome, double subtotalSalary, double isss,
			double afp, double isr, double otherDiscount, Integer idCompany, double totalIncome, double totalDiscount,
			double totalSalary, String observation, boolean enable, List<PayrollDetailModel> details) {
		super();
		this.payrollDate = payrollDate;
		this.salary = salary;
		this.otherIncome = otherIncome;
		this.subtotalSalary = subtotalSalary;
		this.isss = isss;
		this.afp = afp;
		this.isr = isr;
		this.otherDiscount = otherDiscount;
		this.idCompany = idCompany;
		this.totalIncome = totalIncome;
		this.totalDiscount = totalDiscount;
		this.totalSalary = totalSalary;
		this.observation = observation;
		this.enable = enable;
		this.details = details;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getPayrollDate() {
		return payrollDate;
	}

	public void setPayrollDate(Date payrollDate) {
		this.payrollDate = payrollDate;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public double getOtherIncome() {
		return otherIncome;
	}

	public void setOtherIncome(double otherIncome) {
		this.otherIncome = otherIncome;
	}

	public double getSubtotalSalary() {
		return subtotalSalary;
	}

	public void setSubtotalSalary(double subtotalSalary) {
		this.subtotalSalary = subtotalSalary;
	}

	public double getIsss() {
		return isss;
	}

	public void setIsss(double isss) {
		this.isss = isss;
	}

	public double getAfp() {
		return afp;
	}

	public void setAfp(double afp) {
		this.afp = afp;
	}

	public double getIsr() {
		return isr;
	}

	public void setIsr(double isr) {
		this.isr = isr;
	}

	public double getOtherDiscount() {
		return otherDiscount;
	}

	public void setOtherDiscount(double otherDiscount) {
		this.otherDiscount = otherDiscount;
	}

	public Integer getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(Integer idCompany) {
		this.idCompany = idCompany;
	}

	public double getTotalIncome() {
		return totalIncome;
	}

	public void setTotalIncome(double totalIncome) {
		this.totalIncome = totalIncome;
	}

	public double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public double getTotalSalary() {
		return totalSalary;
	}

	public void setTotalSalary(double totalSalary) {
		this.totalSalary = totalSalary;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public List<PayrollDetailModel> getDetails() {
		return details;
	}

	public void setDetails(List<PayrollDetailModel> details) {
		this.details = details;
	}

	@Override
	public String toString() {
		return "PayrollModel [id=" + id + ", payrollDate=" + payrollDate + ", salary=" + salary + ", otherIncome="
				+ otherIncome + ", subtotalSalary=" + subtotalSalary + ", isss=" + isss + ", afp=" + afp + ", isr="
				+ isr + ", otherDiscount=" + otherDiscount + ", idCompany=" + idCompany + ", totalIncome=" + totalIncome
				+ ", totalDiscount=" + totalDiscount + ", totalSalary=" + totalSalary + ", observation=" + observation
				+ ", enable=" + enable + ", details=" + details + "]";
	}

	
	
	
	

}
