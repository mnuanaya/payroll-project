package com.htc.payroll.batchpayrollservice.model;

import java.io.Serializable;
import java.util.List;

import com.htc.payroll.batchpayrollservice.dto.PayrollDto;

public class PayrollList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<PayrollDto> listaNominas;

	public void setListaNominas(List<PayrollDto> listaNominas) {
		this.listaNominas = listaNominas;
	}
	
	

}
