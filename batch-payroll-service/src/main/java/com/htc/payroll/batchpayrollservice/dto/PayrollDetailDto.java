package com.htc.payroll.batchpayrollservice.dto;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class PayrollDetailDto {
	
	private Integer id;
	private Integer idPayroll;
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone="America/El_Salvador")
	private Date payrollDate;
	private Integer idPayrollType;
	private double nominalSalary;
	private double otherIncome;
	private double otherDiscount;
	private double isss;
	private double afp;
	private double isr;
	private String vigencyDiscounts;
	private String vigencyRetention;
	private Integer idEmployee;
	private Integer idCompany;
	private double totalIncome;
	private double totalDiscount;
	private double totalSalary;

}
