package com.htc.payroll.batchpayrollservice.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.htc.payroll.batchpayrollservice.model.PayrollDetailModel;


public class PayrollDto implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@NotNull(message="Empresa no puede ser nula")
	private Integer idCompany;
	
	
	private List<PayrollDetailModel> details;
	
	

	public PayrollDto() {
		super();
	}



	public PayrollDto(@NotNull(message = "Empresa no puede ser nula") Integer idCompany,
			List<PayrollDetailModel> details) {
		super();
		this.idCompany = idCompany;
		this.details = details;
	}



	public Integer getIdCompany() {
		return idCompany;
	}



	public void setIdCompany(Integer idCompany) {
		this.idCompany = idCompany;
	}



	public List<PayrollDetailModel> getDetails() {
		return details;
	}



	public void setDetails(List<PayrollDetailModel> details) {
		this.details = details;
	}


	
	
	
//private static final long serialVersionUID = 1L;
//
//
//	
//	
//	private List<PayrollDetailModel> details;
//	
//	
//
//	public PayrollDto() {
//		super();
//	}
//
//
//
//	public PayrollDto(
//			List<PayrollDetailModel> details) {
//		super();
//		this.details = details;
//	}
//
//
//
//
//	public List<PayrollDetailModel> getDetails() {
//		return details;
//	}
//
//
//
//	public void setDetails(List<PayrollDetailModel> details) {
//		this.details = details;
//	}
//
//
//	
//	
	
	
	
	
	

}


