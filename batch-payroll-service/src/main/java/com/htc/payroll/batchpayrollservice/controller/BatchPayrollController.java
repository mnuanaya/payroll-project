package com.htc.payroll.batchpayrollservice.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.htc.payroll.batchpayrollservice.client.PayrollClient;
import com.htc.payroll.batchpayrollservice.dto.PayrollDto;
import com.htc.payroll.batchpayrollservice.exception.EmptyException;
import com.htc.payroll.batchpayrollservice.exception.NullObjectException;
import com.htc.payroll.batchpayrollservice.exception.OperationException;
import com.htc.payroll.batchpayrollservice.model.PayrollDetailModel;
import com.htc.payroll.batchpayrollservice.model.PayrollModel;
import com.htc.payroll.batchpayrollservice.service.BatchPayrollService;
import com.htc.payroll.batchpayrollservice.util.Constants;




@RestController
@RequestMapping("/batch")
public class BatchPayrollController {
	
	@Autowired
	Environment env;
	
	@Autowired
	BatchPayrollService batchPayroll;
	
	@Autowired
	  Mapper mapper;
	
	@Autowired
	PayrollClient payrollClient;

	
	@PostMapping(path = "/")
	ResponseEntity<Object> insertPayroll(@RequestParam String filePathpayroll) throws OperationException, NullObjectException, EmptyException {
		
		HttpHeaders headers = new HttpHeaders();
		
		
		PayrollDto payrollDto = new PayrollDto();
		Boolean res;
		//PayrollDto listaNominas = new ArrayList<>();
		res = batchPayroll.fileList(filePathpayroll);

		
		//payrollDto= batchPayroll.fileList(filePathpayroll);
	
		
		//System.out.println(payrollDto.toString());
			
			
//			payrollClient.insertPayroll(payrollDto);
			
			
				
		headers.add(String.valueOf(env.getProperty(Constants.MESSAGE_SUCCESS)),
				env.getProperty(Constants.SUCCESS_CODE));

		return new ResponseEntity<>(res,headers, HttpStatus.OK);
		//return payDto;
	}
	
//	@PostMapping(path = "/")
//	PayrollModel insertPayroll(@RequestParam String filePathpayroll) throws OperationException, NullObjectException, EmptyException {
//		
//		HttpHeaders headers = new HttpHeaders();
//		
//		
//		/*if(filePathpayroll == null || filePathpayroll.isEmpty()) {
//			
//			
//		}*/
//		
//		Map<String, String> uriVariables = new HashMap<>();
//		uriVariables.put("filePathpayroll", filePathpayroll);
//
//
//		ResponseEntity<PayrollModel> responseEntity = new RestTemplate().getForEntity(
//				"http://localhost:8092/payroll/", PayrollModel.class,
//				uriVariables);
//
//		PayrollModel response = responseEntity.getBody();
//		
//		/*PayrollDto payrollDto = new PayrollDto();
//		PayrollDetailModel detallePay = new PayrollDetailModel();
//		
//		PayrollModel payDto = mapper.map(payrollDto, PayrollModel.class);
//		
//		List<PayrollDetailModel> listaDetalles = new ArrayList<>();
//		
//		
//		listaDetalles= batchPayroll.fileList(filePathpayroll);
//	
//		payrollDto.setDetails(listaDetalles);
//		payrollDto.setIdCompany(2);
//		
//		System.out.println(payrollDto.toString());
//			
//			
//			payrollClient.insertPayroll(payrollDto);*/
//			
//			
//				
//		headers.add(String.valueOf(env.getProperty(Constants.MESSAGE_SUCCESS)),
//				env.getProperty(Constants.SUCCESS_CODE));
//
//		//return new ResponseEntity<>(headers, HttpStatus.OK);
//		return new PayrollModel(response.getDetails());
//	}
	

}
